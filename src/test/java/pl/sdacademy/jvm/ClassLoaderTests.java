package pl.sdacademy.jvm;

import org.junit.Test;

import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;

public class ClassLoaderTests {

    @Test
    public void URLClassLoaderTest() throws Exception {
        System.out.println("Working directory: " + System.getProperty("user.dir"));

        URL classDir = Paths.get("plugin").toUri().toURL();
        System.out.println("Plugin classes directory: " + classDir.getPath());

        // Load class
        ClassLoader urlClassLoader = new URLClassLoader(new URL[]{classDir});
        Class<?> cl = urlClassLoader.loadClass("pl.sdacademy.plugin.PluginService");

        // Create pl.sdacademy.plugin.PluginService class instance
        Object pluginService = cl.newInstance();

        // Invoke 'pluginService' objects 'doService' method
        Method method = cl.getMethod("doService");
        String html = (String) method.invoke(pluginService);
        System.out.println("Plugin output: " + html);
    }
}
